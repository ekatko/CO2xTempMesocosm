checkT0 <- function() {
  library(tidyverse)
  source("R/Helpers/lm_wrapper.R", encoding="UTF-8")
  df <- getCompiledData()
  # Check for any significant trends in time zero data
  df_T0 <- 
    df %>% 
    filter(Date < "2020-10-08", CO2 != "LAC") %>%
    drop_units()
  
  for(r in list("CO2_calc", "Temp", "Total.conc.", "Green.Algae", "Bluegreen", "Diatoms")) {
    lm_wrapper(resp = r, data=df_T0) %>%
      print
    readline(prompt="Press [enter] to continue")
  }
}
### DEBUG
checkT0()
# Note: Total.conc. exhibits a trend with CO2 (p=0.06)!