source("R/DataGetters/LoadDataGetters.R")
calcStoich <- function(df = getCompiledData()) {
  df %>% tibble() %>%
    # select(Mesocosm, Date, Day, Temperature, CO2, CO2_calc, TP, DP, TN, DN, `%C`, `%N`) %>%
    mutate(
      ## Calculate Total & dissolved stoichiometry
      `Dissolved.Molar.N.P` = DN/DP,
      `Total.Molar.N.P` =     TN/TP,
      
      ## Calculate Particulate / seston stoichiometry
      PP = TP-DP,
      PN = TN-DN, 
      PC = PN * `%C` / `%N`,
      PPmol = PP / set_units(30.9738, g/mol),
      PNmol = PN / set_units(14.0067, g/mol),
      PCmol = PC / set_units(12.0111, g/mol), 
      `Seston Molar C:N` = PCmol/PNmol,
      `Seston Molar C:P` = PCmol/PPmol
    ) %>%   
    ## Drop extreme outlier
    filter(! (Mesocosm==12 & Day == 19)) %>% 
    mutate(Seston.Molar.C_N = drop_units(`Seston Molar C:N`))
  
}
