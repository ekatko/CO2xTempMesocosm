source("R/DataGetters/getCompiledData.R", encoding = "UTF-8")
library(tidyverse)
library(units) # For dropping units
testDailyResp_lm <- function(df=getCompiledData(lake=F, units=F), r, log=F) {
  df = df %>% drop_units() # Just in case
  # browser()
  days = df%>% 
    drop_na(!! r) %>% 
    arrange(Day) %>% pull(Day) %>% unique
  if(log) {
    r = paste0("log(", r,")")
  }
  formula = as.formula(paste(r,"~CO2*Temperature"))
  
  days %>%
    lapply(function(d) {
      lm = df %>% 
        filter(Day == !! d) %>%
        lm(formula, data = .)
      a = lm %>% anova %>% broom::tidy()
      
      list(
        day=d,
        lm =lm,
        anova = lm %>% anova()
      )
    })
}

## DEBUG
# testDailyResp_lm(r="Chla")

getAnnotation <- function(df=getCompiledData(lake=F, units=F), r) {
  source("R/Plotting/Palette.R")
  # browser()
  l = testDailyResp_lm(df, r)
  c("Temperature", "CO2") %>%
    lapply(function(t) {
      
      s = 
        l %>% sapply(function(d){
          # browser()
          # print(d$day)
          bool = d$anova %>% broom::tidy() %>% filter(term==t) %>% pull(p.value) %>% `<=`(0.05)
          names(bool) = d$day
          bool 
        })
      an = annotate("text", 
                    x=as.numeric(names(s)), 
                    y= assign_y_signif(df, r), 
                    label=assign_signif_symb(s))
    })
}
# getAnnotation(r="Chla")
# 