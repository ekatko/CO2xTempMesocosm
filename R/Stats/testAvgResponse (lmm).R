source("R/DataGetters/getCompiledData.R", encoding = "UTF-8")
library(lme4)
library(lmerTest)
library(tidyverse)
library(kableExtra)
getCompiledData(lake=F, units=F, long=T) %>%
  filter(Day > 0) %>% 
  mutate(Half = if_else(Day < mean(unique(Day))/2, "First", "Second")) %>%
  group_by(Mesocosm, name, Half) %>%
  summarise(Temperature = unique(Temperature), CO2 = unique(CO2), value = mean(value, na.rm=T)) %>% 
  pivot_wider()  -> df

lm(Chla~ Temperature*CO2, data=df %>% filter(Half=="Second")) %>% summary

