library(party)
?ctree
source("R/Helpers/ManuscriptFunctions.R", local = knitr::knit_global(), encoding="UTF-8")
getCompiledData(lake=F) %>% filter(!is.na(Chla)) %>%
  group_by(Day) %>%
  mutate(ChlaDailyMean = mean(Chla), 
         ChlaDiffFromDailyMean = Chla - mean(Chla)) %>% filter(CO2!="Mid")-> dfLOL

dfLOL%>% 
  ctree(ChlaDiffFromDailyMean ~ as.factor(Day) * CO2 *Temperature  + (1 | Mesocosm), 
        data=., 
        controls = ctree_control(
          mincriterion =0.8,
          testtype="Univariate")) %>% plot

testDailyResp(df = dfLOL, r = "ChlaDiffFromDailyMean")

plotTimeSeries(dfLOL,r="ChlaDiffFromDailyMean", dataPts=T)
plotTimeSeries()