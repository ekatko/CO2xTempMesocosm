source("R/DataGetters/getFPData.R", encoding="UTF-8")
source("R/DataGetters/getHOBOData.R", encoding="UTF-8")
source("R/DataGetters/getSheetData.R", encoding="UTF-8")
source("R/DataGetters/getTreatmentMapping.R", encoding="UTF-8")
source("R/DataGetters/getNutrientData.R", encoding="UTF-8")
source("R/DataGetters/getFilterNutrientData.R", encoding="UTF-8")
source("R/DataGetters/getZoopData.R", encoding="UTF-8")
source("R/DataGetters/getZoopBiomass.R", encoding="UTF-8")

