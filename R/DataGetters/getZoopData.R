## Extract date and MesocosmID from the "Sample" column.
formatZoopSheet <- function(df, wide, lac, na_to_zero) {
  source("R/DataGetters/getTreatmentMapping.R", encoding="UTF-8")
  df = df %>%  
    mutate(
      Date =      
        str_split(Sample, "-", simplify=T)[,1] %>% 
        trimws %>% 
        gsub("TO", "Oct 06", .) %>% 
        paste("2020") %>%
        as.Date(Date, format = "%b %d %Y"),
      Mesocosm = str_split(Sample, "-", simplify=T)[,2] %>% trimws, 
      Mesocosm = gsub("LAK", "LAC", Mesocosm, fixed=T)
    ) %>%
    filter(Mesocosm != "") %>%
    select(Mesocosm, Date, any_of(c("Replicate", "Notes")), where(is.numeric) ) %>% 
    merge(getTreatmentMapping(), ., all=T)
  if(na_to_zero) { 
    df = df %>% mutate(across(where(is.numeric), ~coalesce(.x, 0) ))
  }
  if(!lac)  {df = df %>% filter(CO2 != "LAC")}
  if(wide) {return(df)}
  df %>% pivot_longer(cols= where(is.numeric), 
                      names_to = "Species", values_to="Count" )
}

## Correct rotifer counts to make them per sample
getZoopData <- function(wide=T, lac=F, na_to_zero=T, summarise_replicates=T, combine=F, combineConochilus=F ){
  library(tidyverse)
  library(readxl)
  Rotifers = 
    read_excel("Data/Egor zooplankton counts.xlsx", skip=3) %>%  
    mutate(Replicate = as.factor(Replicate), 
           across(where(is.numeric), ~./0.05)) %>% # 5% of volume sub-samples were counted for Rotifers
    formatZoopSheet(wide, lac, na_to_zero)
  
  if(combineConochilus) {
    Rotifers = Rotifers %>%
      mutate(`Conochilus unicornis` = `Conochilus unicornis (colony)` + `Conochilus unicornis (individuals)`) %>%
      select(-`Conochilus unicornis (colony)`, -`Conochilus unicornis (individuals)`)
  }

  if(combine & !summarise_replicates) {
    warning("Replicates must be summarised to combine both data frames.")
    summarise_replicates=T
  }
  
  if(summarise_replicates && wide) {
    Rotifers = Rotifers %>% 
      group_by(Mesocosm, Date, CO2, Temperature) %>%
      summarise(across(where(is.numeric), mean), Notes = paste(Notes, collapse=" && ")) %>%
      ungroup
  } else if(summarise_replicates && !wide ) {
    Rotifers = Rotifers %>% 
      group_by(Mesocosm, Date, Species, CO2, Temperature) %>%
      summarise(Count = mean(Count), Notes = paste(Notes, collapse=" && ")) %>%
      ungroup
  }
  
  Crustaceans = 
    read_excel("Data/Egor zooplankton counts.xlsx", sheet=2) %>% 
    formatZoopSheet(wide, lac, na_to_zero)
  
  if(combine & !wide) {
    Crustaceans = Crustaceans %>% mutate(group = "Crustacean", Notes="")
    Rotifers    = Rotifers    %>% mutate(group = "Rotifers")
    return(rbind(Rotifers, Crustaceans))
  } else if(combine & wide) {
    Rotifers    = Rotifers    %>% select(-Notes)
    return(full_join(Crustaceans, Rotifers))
  }
  list(Rotifers=Rotifers, Crustaceans=Crustaceans)
}


## DEBUG
# Check which dates are in the dataset
# library(tidyverse)
# getZoopData(wide=T, combine=T, combineConochilus=T) %>% tibble()
# getZoopData(wide=F, combine=T) %>% tibble()
# getZoopData(wide=T, combine=F) 

